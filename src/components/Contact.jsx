import React from 'react'
import "./Contact.css"


 function Contact(props) {

  let dotClass=""

  if (props.online){
    dotClass="online"
  }


  return (
    <>
    <h1> {props.contactName} </h1>
    <div className='container'>
      <div className={"connectionGreen " + dotClass}> </div>
    <img className='img' src={props.contactImage} alt="portrait utilisateur" />
    
    </div>
        
  
    </>
    
  )
}
export default Contact
